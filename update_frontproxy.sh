docker-compose stop
docker pull jwilder/nginx-proxy:alpine
docker pull jrcs/letsencrypt-nginx-proxy-companion
docker-compose up --build -d
docker rmi $(docker images | grep none)