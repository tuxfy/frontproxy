# nginx reverse proxy + letsencrypt container
https://techblog.sitegeist.de/docker-compose-setup-mit-nginx-reverse-proxy/

```shell
# docker-compose up --build -d
```

erstellt ein network frontproxy_default   
= abhängig vom ordnernamen !!!  
see:
```shell
# docker network ls
```

# Beispiel docker-compose für services

```shell
# cd ..
# mkdir project && cd project
# vi docker-compose.yml
```

Die VIRTUAL_HOST - Paramter wie gewünscht anpassen  

```yaml
version: '2'
    services:
      web:
        image: apache:php5.6
        hostname: apache
        container_name: apache
        volumes:
          - /home/ruges/docker/apache/:/var/www/html
          - /home/ruges/docker/apache_log/:/var/log/apache2
        networks:
        - frontproxy_default
        - default
        environment:
          - VIRTUAL_HOST=apache.localhost
      web2:
        image: apache:php5.6
        hostname: apache2
        container_name: apache2
        volumes:
          - /home/ruges/docker/apache2/:/var/www/html
          - /home/ruges/docker/apache_log2/:/var/log/apache2
        networks:
        - frontproxy_default
        - default
        environment:
          - VIRTUAL_HOST=apache2.localhost
      db:
        image: mysql:5.6
        hostname: mysql
        container_name: mysql
        environment:
          - MYSQL_ROOT_PASSWORD=password
        volumes:
          - /home/ruges/docker/mysql5.6/:/var/lib/mysql/
        networks:
          - frontproxy_default
          - default
      myadmin:
        image: phpmyadmin/phpmyadmin
        hostname: myadmin
        container_name: myadmin
        environment:
          - MYSQL_ROOT_PASSWORD=password
        networks:
          - frontproxy_default
          - default
        environment:
          - VIRTUAL_HOST=myadmin.localhost
    networks:
       frontproxy_default:
         external: true
```

## letsencrypt

https://github.com/JrCs/docker-letsencrypt-nginx-proxy-companion

für letsencrypt müssen die folgene ENV - Parameter hinzugefügt werden

  - VIRTUAL_HOST=example.com,www.example.com,mail.example.com
  - LETSENCRYPT_HOST=example.com,www.example.com,mail.example.com
  - LETSENCRYPT_EMAIL=foo@bar.com

Die VIRTUAL_HOSTs müssen für letsencrypt erreichbar sein und den LETSENCRYPT_HOST entsprechen!!!  
Die einzelnen Container müssen den Port 443 nach außen freigeben (EXPOSE 443)

für Testzertifikat

  - LETSENCRYPT_TEST=true





```shell
# docker-compose up --build -d
```
